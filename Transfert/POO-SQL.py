""" ---------------- Module "AutoSQL" ---------------- """
#Projet POO SQL
#auteurs Dominique / Julien / Hamid

import psycopg2, sqlite3

""" -------- Coin du dev -------- """
def wip(WipNote): # Affichage de "Work in progress" 
    if WipNote == 0:
        print("Développement en cours !")
    else:
        print("Développement en cours : " + str(WipNote))

""" -------- Classes -------- """

class dba : # Méthode de création par défaut pour les néophites ou pour les experts (URI)
    def __init__(self, NomBD, URI=None, TypeBD="S"):
        self.NomDB = NomBD
        self.URI = URI
        self.TypeBD = TypeBD

        if self.TypeBD == "S":
            dbs(self.NomDB, self.URI, self.TypeBD)

        elif self.TypeBD == "P":
            wip("Postgres Type Error")

        else:
            ErrorAutoSQL()


class dbs(dba) : # Méthode de création de BD par SQLite3 (Par défaut)
    '''
    :parametres : nom_DB_fichier [en option : URI, le Type de BD (S ou P), le chemin d'acces à nom_DB_fichier ]
    '''
    def __init__(self, NomDB, URI=None, TypeBD="S", CheminBD=None):
        #super().__init__()
        self.NomDB = NomDB
        self.URI = URI
        self.CheminBD = CheminBD
        
        if CheminBD == None:
            self.FichierBD = NomDB + ".db"
        else:
            self.FichierBD = CheminBD + NomDB + ".db"
        
        self.Connecteur = self.MaCreationDeConnexionSQLite(self.FichierBD)

        
    def MaCreationDeConnexionSQLite(self, UnFichierBD): # Méthode appelée automatiquement par le constructeur
        MonConnecteur = None
        try:
            MonConnecteur = sqlite3.connect(UnFichierBD)
        except sqlite3.Error as UneErreurDeConnexion:
            print(UneErreurDeConnexion)
        return MonConnecteur
    
    
    def create_table(self,nom_Table,*args):
        '''
        Création de Tables dans la DATABASE identifiée par -self- 
        : parametres : nom_table = le nom de la table (type STR) - ex: "T_utilisateurs"
                       *args = la liste des champs et leurs propriétés - 
                               ex: 'INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE', 'NOM_Util TEXT', ...
        : resultat : une table
        '''
        if (self.Connecteur):
            try:
                uneTable=Table(nom_Table, *args)  # instanciation de la classe Table
                cur = self.Connecteur.cursor()
                cur.executescript(uneTable.creation_sql)
        
            except Error as e:
                print(e)
    
       

class dbp(dba) : 
    wip("Postgres DB") # Méthode de création de BD Postgres


class Table:
    '''
    creation de Tables  
    :Paramétres : nom_table (type STR) - ex: "T_utilisateurs"
                  *args (liste des champs et leur type, clé primaire ... empaquetés)
    '''
    def __init__(self, nom_table,*args):
        '''
        constructeur de TABLE
        attributs: ceux de la méthode create_table [le nom de TABLE, 
                   le script des instructions SQL de creation de TABLE (DROP si existe et CREATE)]
                  
        '''
        self.nom=nom_table
        
        #construction du script de la chaine creation de table a partir des arguments champs fournis
        #test l'existence de la table et la DROP si existe
        self.creation_sql=f'''DROP TABLE IF EXISTS {self.nom};
                            CREATE TABLE IF NOT EXISTS {self.nom}('''
        for idx, chp in enumerate(args):
            print(chp)
            if idx < len(args)-1 :                
                self.creation_sql += chp + ", "
            elif idx == len(args)-1 :
                self.creation_sql += chp 
        self.creation_sql += ");"
        
        
    def delete_table(self):
        '''
        '''
        print('delete_t')
        
    def insert_data(self):
        '''
        '''
        print('insert_d')
        
    def read_data(self):
        '''
        '''
        print('consulter_d')
        
    def delete_data(self):
        '''
        '''
        print('del_d')  

""" -------- Autres -------- """


def ErrorAutoSQL(): # WIP interprétateur d'erreurs d'utilisation du module "AutoSQL"
    print("Erreur AutoSQL")

def HelpAutoSQL(): # WIP Idée saugrenue...
    #Terminal = input()
    if Terminal == "autosql --help":
        wip("Aide module AutoSQL")
        print("""
dba(NomBD, URI='Optionel') : Méthode de création rapide par défaut pour les néophites ou pour les experts (URI).
    Pour les débutants, AutoSQL crée une BD SQLite3 dans votre dossier actuel.
    Pour les expérts, une URI (optionelle) permet de configurer votre BD rapidement.

dbs(NomDB, CheminBD='Optionel') : Méthode de création de BD SQLite3 (Par défaut)
    CheminBD perment de définir l'emplacement du fichier NomDB.db dans votre arborescence.

dbp(NomDB, host, port, user, password) : Méthode de création de BD Postgres > WIP""")
    else:
        pass


#main
#test
maBaseTest=dbs("BaseEssai")

print('base créée')

dbs.create_table(maBaseTest,"maTableEssai","ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE",
    "Id_dpt TEXT","nom_commune TEXT","code_postal TEXT","code_insee TEXT","latitude REAL","longitude REAL")