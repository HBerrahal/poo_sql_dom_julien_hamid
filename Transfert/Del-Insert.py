def delete_table(self,connecteurDB):
        '''
        '''
        self.supp_sql=f"DROP TABLE IF EXISTS {self.nom};"
        if (connecteurDB):
            try:
                cur = connecteurDB.cursor()
                cur.execute(self.supp_sql)
        
            except Error as e:
                print(e)
        print(f'delete_t: ',{self.nom})
 


    def insert_data(self,connecteurDB,*args):
        '''args: tuple de liste de dictionnaire [{"col1":1, "col2":"test","col3":4},
                                    {"col1":1, "col2":"test","col3":4}]
        '''
        #construction du script de la chaine d'insertion de données a partir 
        #des arguments champs - valeurs fournis
        self.insert_sql=f'''INSERT INTO {self.nom} ('''
        print(self.insert_sql)
        #instruction SQL correspondant à l'insertion d'un enregitrement dans la Table
        #extraction des nom de champs donnés par le nom des cles du 1er dico de 
        #la liste argument
        
        pti="?,"*(len(args[0][0])-1)
                
        i=1
        ligne_data=[]
        for k, v in enumerate(args[0][0]): 
            if i < len(args[0][0]) : 
                self.insert_sql += v + ", " #les noms de champs
            elif i == len(args[0][0]) :
                self.insert_sql += v
            ligne_data.append(args[0][0][v])   #liste des valeurs a inserer
            i+=1
        self.insert_sql += f''') VALUES({pti}?);'''
        tpl_data=tuple(ligne_data)
        print("chaine : ",self.insert_sql)
        print("values : ",tpl_data)
        
        
        cur = connecteurDB.cursor()
        cur.execute(self.insert_sql,tpl_data)
        #nb_ajout += cur.rowcount
        #valide l'insertion cur.execute()   
        connecteurDB.commit()
        print('insert_data')