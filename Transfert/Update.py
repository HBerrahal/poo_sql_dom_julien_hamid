def update_data(self,connecteurDB, col_filtre, val_filtre, *nvl_data ):
        '''ex : table.update_data("col_1", 5,{"col_1" : 2,
                                              "col_2" : "test2",
                                              "col_3" : 8})
        
        Met a jour les champs de la table avec de nvles valeurs
        pour l'enregistrement filtré sur le champs donné par 'col_filtre' dont la valeur est 'val_filtre'
        :param = col_filtre, val_filtre : enregistrement à mettre à jour
                 entre { } : (structure de dictionnaire) "champ_a_modifier" : nouvelle_valeur
        :return:
    
        '''
        set_val=""
        i=1
        for k in nvl_data[0]:
            if i < len(nvl_data[0]) : 
                set_val += k + " = '"+ str(nvl_data[0][k]) + "', "  if type(nvl_data[0][k])==str else k + " = "+ str(nvl_data[0][k]) + ", "
            elif i == len(nvl_data[0]) :
                set_val += k + " = '"+ str(nvl_data[0][k]) + "', "  if type(nvl_data[0][k])==str else k + " = "+ str(nvl_data[0][k])
            i+=1
        
        update_sql = f'UPDATE {self.nom} SET {set_val} WHERE {col_filtre} = '
        update_sql += f'"{val_filtre}";' if type(val_filtre) ==str else f'{val_filtre};'
        
        cur = connecteurDB.cursor()
        cur.execute(update_sql)