def delete_data(self, connecteurDB, col_filtre, val_filtre):
        '''ex : table.delete_data("col_1", 5)
        
        Supprime tous les enregistrements de la table
        filtrée sur le champs donné par 'col_filtre' dont la valeur est 'val_filtre'
        :param :
        :return:
        
        '''
        delete_sql = f'DELETE FROM {self.nom} WHERE {col_filtre} = '
        delete_sql += f'"{val_filtre}";' if type(val_filtre) ==str else f'{val_filtre};'
        
        
        cur = connecteurDB.cursor()
        cur.execute(delete_sql)
        print('del_d')  