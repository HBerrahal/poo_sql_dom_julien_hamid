import sqlite3

class Create_DB: # Méthode de création de BD par SQLite3 (Par défaut)
    '''
    :parametres : nom_DB_fichier(sans extension .db) 
                [en option : URI, le Type de DB (S -sqlite3 par défaut- ou P), 
                le chemin d'acces à nom_DB_fichier ]
    '''
    def __init__(self, NomDB, URI=None, TypeDB="S", CheminDB=None):
        self.NomDB = NomDB
        self.URI = URI
        self.TypeDB = TypeDB
        self.FichierDB = NomDB + ".db" if CheminDB == None else CheminDB + NomDB + ".db"
        self.ConnecteurDB = self.create_connexion_sqlite3(self.FichierDB) if self.TypeDB == "S" else None
        
    def create_connexion_sqlite3(self, NomFichierDB): 
        # Méthode appelée automatiquement par le constructeur si sqlite3
        self.ConnecteurDB = None
        try:
            self.ConnecteurDB = sqlite3.connect(NomFichierDB)
        except sqlite3.Error as UneErreurDeConnexion:
            print(UneErreurDeConnexion)
        return self.ConnecteurDB
    
    def create_connexion_postg(self, NomFichierDB):                        #a developper
        # Méthode appelée automatiquement par le constructeur si postgres
        self.ConnecteurDB = None
        try:
            self.ConnecteurDB = None# sqlite3.connect(NomFichierDB)
        except sqlite3.Error as UneErreurDeConnexion:
            print(UneErreurDeConnexion)
        return self.ConnecteurDB
    
    
    def create_table(self,nom_Table,*args):
        '''
        Création de Tables dans la DATABASE identifiée par -self- 
        : parametres : nom_table = le nom de la table (type STR) - ex: "T_utilisateurs"
                       *args = la liste des champs et leurs propriétés - 
                               ex: 'INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE', 'NOM_Util TEXT', ...
        : resultat : une table
        '''
        
        if (self.ConnecteurDB):
            try:
                uneTable=Table(nom_Table, *args)  # instanciation de la classe Table
                cur = self.ConnecteurDB.cursor()
                cur.executescript(uneTable.creation_sql)
        
            except Error as e:
                print(e)

class Table:
    '''
    creation de Tables  
    :Paramétres : nom_table (type STR) - ex: "T_utilisateurs"
                  *args (liste des champs et leur type, clé primaire ... : empaquetés)
    '''
    def __init__(self, nom_table,*args):
        '''
        constructeur de TABLE
        attributs: le nom de TABLE, 
                   le script des instructions SQL de creation de TABLE (DROP si existe et CREATE) 
                   composé a partir de *args
                  
        '''
        self.nom=nom_table
        
        #construction du script de la chaine creation de table a partir des arguments champs fournis
        #test l'existence de la table et la DROP si existe
        self.creation_sql=f'''DROP TABLE IF EXISTS {self.nom};
                            CREATE TABLE IF NOT EXISTS {self.nom}('''
        for idx, chp in enumerate(args):
            print(chp)
            if idx < len(args)-1 :                
                self.creation_sql += chp + ", "
            elif idx == len(args)-1 :
                self.creation_sql += chp 
        self.creation_sql += ");"
        
        
    def delete_table(self):
        '''
        '''
        print('delete_t')
        
    def insert_data(self):
        '''
        '''
        print('insert_d')
        
    def read_data(self):
        '''
        '''
        print('consulter_d')
        
    def delete_data(self):
        '''
        '''
        print('del_d')  

""" -------- Autres -------- """


def ErrorAutoSQL(): # WIP interprétateur d'erreurs d'utilisation du module "AutoSQL"
    print("Erreur AutoSQL")

def HelpAutoSQL(): # WIP Idée saugrenue...
    #Terminal = input()
    if Terminal == "autosql --help":
        wip("Aide module AutoSQL")
        print("""
Create_DB(NomDB, URI='Optionel') : Méthode de création rapide par défaut pour les néophites ou pour les experts (URI).
    Pour les débutants, AutoSQL crée une BD SQLite3 dans votre dossier actuel.
    Pour les expérts, une URI (optionelle) permet de configurer votre BD rapidement.

Create_DB(NomDB, CheminDB='Optionel') : Méthode de création de BD SQLite3 (Par défaut)
    CheminDB perment de définir l'emplacement du fichier NomDB.db dans votre arborescence.

Create_DB(NomDB, host, port, user, password) : Méthode de création de BD Postgres > WIP""")
    else:
        pass

# main 
# test

mabase=Create_DB("essai")
print(mabase.NomDB)
print(mabase.URI)
print(mabase.TypeDB)
print(mabase.FichierDB)
print(mabase.ConnecteurDB)
print('base créée')

mabase.create_table(
    "maTableEssai","ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE",
    "Id_dpt TEXT","nom_commune TEXT","code_postal TEXT","code_insee TEXT","latitude REAL","longitude REAL"
)