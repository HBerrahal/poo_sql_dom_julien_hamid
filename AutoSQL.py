""" ---------------- Module "AutoSQL" ---------------- """
# Devellopé par Dominique Tatin, Hamid Berrahal & Julien Stacchetti

import psycopg2, sqlite3, re

""" -------- Coin du dev -------- """
def wip(WipNote = None): # Affichage de "Work in progress" 
    if WipNote == None:
        print("Développement en cours !")
    else:
        print("Développement en cours : " + str(WipNote))

""" -------- Classes -------- """
class dba : # Classe/Méthode de création par défaut pour les néophytes ou pour les experts (URI)
    '''
    Exemple : dba(NomBD)
    Exemple : dba(URI=sqlite:////MonChemin/MaDB.db)
    Méthode de création rapide par défaut.
    Pour les néophytes, AutoSQL crée une BD SQLite3 dans votre dossier actuel.
    Pour les expérts, une URI permet de configurer votre BD rapidement.

    :parametres : NomBD (Sans l'extension .db) OU URI= (MonUri) 
    '''
    def __init__(self, NomBD="MaBDTest", URI=None, TypeBD="S"): # Constructeur des objets de classe dba
        self.NomBD = NomBD
        self.URI = URI
        self.TypeBD = TypeBD

        if self.URI != None:
            if re.findall("(?=sqlite:////).+(?<=\.db)", self.URI):
                self.Connecteur = dbs(self.NomBD, self.URI, self.TypeBD).Conn()

            elif re.findall("^postgresql://", self.URI):
                self.TypeBD = "P"
                # postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]
                self.Connecteur = dbp(NomBD=None, host=None, port=None, user=None, password=None, URI=self.URI, TypeBD="P").Conn()

            else:
                ErrorAutoSQL()
        
        else:
            self.Connecteur = dbs(self.NomBD, self.URI, self.TypeBD).Conn()


    def tab(self, NomTab, *args): # Méthode de création d'objets de classe Table
        '''
        Création de Tables dans la DATABASE identifiée par -self- 
        : parametres : NomTab = le nom de la table (type STR) - ex: "T_utilisateurs"
                       *args = la liste des champs puis de leurs propriétés - 
                               ex:  'ID', 'INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE',
                                    'NOM_Util', 'TEXT',
                                    ...
        : resultat : un objet table dans mon objet BD
        '''
        self.cur = self.Connecteur.cursor()
        uneTable = table(NomTab, *args, TypeBD=self.TypeBD, Connector=self.Connecteur, Cursor=self.cur)  # instanciation de la classe Table
        self.cur.executescript(uneTable.CodeSQL)
        return uneTable


class dbs(dba) : # Classe/Méthode de création de BD par SQLite3 (Par défaut)
    '''
    Exemple : dbs(NomBD, CheminBD='Optionel')
    Méthode de création de BD SQLite3 (Par défaut)
    CheminBD perment de définir l'emplacement du fichier NomBD.db dans votre arborescence.

    :parametres : NomBD (Sans l'extension .db), CheminBD (local)
    '''
    def __init__(self, NomBD=None, URI=None, TypeBD="S", CheminBD=None): # Constructeur des objets de classe dbs (Par defaut)
        self.NomBD = NomBD
        self.URI = URI
        self.TypeBD = TypeBD
        self.CheminBD = CheminBD
    
        if self.URI != None:
            self.FichierBD = re.sub("^sqlite:///", "", self.URI)
        else:
            if CheminBD == None:
                self.FichierBD = NomBD + ".db"
            else:
                self.FichierBD = CheminBD + NomBD + ".db"
        
        self.Connecteur = self.Conn()
        
    def Conn(self): # Méthode appelée automatiquement, gère la connexion spécifique
        MonConnecteur = None
        try:
            MonConnecteur = sqlite3.connect(self.FichierBD)
        except sqlite3.Error as UneErreurDeConnexion:
            print(UneErreurDeConnexion)
        return MonConnecteur

    
class dbp(dba) : # Classe/Méthode de création de BD Postgres
    '''
    Exemple : dbp(NomBD, host, port, user, password)
    Méthode de création de BD Postgres
            > WIP

    :parametres : NomBD, host, port, user, password...
    '''
    def __init__(self, NomBD=None, host=None, port=None, user=None, password=None, URI=None, TypeBD="P"): # Constructeur des objets de classe dbp
        self.NomBD = NomBD
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.URI = URI
        self.TypeBD = "P"

        # self.Connecteur = self.Conn(self.host, self.port, self.NomBD, self.user, self.password)
        wip("Postgres DB")


    def Conn(self): # Méthode appelée automatiquement, gère la connexion spécifique
        MonConnecteur = psycopg2.connect(host=self.host , port=self.port , database=self.NomBD, user=self.user , password=self.password)
        return MonConnecteur


class table: # Classe de manipulation de Table
    '''
    cf. .tab()
    '''
    def __init__(self, nom_table, *args, TypeBD=None, Connector=None, Cursor=None): # Constructeur d'un objet table + Variation de syntaxe selon le TypeBD
        self.NomTab = nom_table
        self.TypeBD = TypeBD
        self.Connector = Connector
        self.Cursor = Cursor
        self.CodeSQL = f"CREATE TABLE IF NOT EXISTS {self.NomTab}("
        self.NomsChamps = []
        
        for idx, chp in enumerate(args):
            if idx < len(args)-1 :
                if idx%2 == 0:
                    self.NomsChamps.append(chp)
                    self.CodeSQL += chp + " "
                else:
                    self.CodeSQL += chp + ", "
            elif idx == len(args)-1 :
                self.CodeSQL += chp 
        self.CodeSQL += ");"

        # ----- Variation de vocabulaire selon le type de BD -----
        if self.TypeBD == "P":
            self.SQL_Val = "%s"
            wip()
        else: # Option par défaut = SQLite
            self.SQL_Val = "?"


    def col(self): # WIP : Méthode de consultation des colonnes de la table
        '''
        Exemple : MaTable.col()
        Enumére dans le terminal le nom des champs de l'object table.
        '''
        #self.Cursor.executescript(f"""SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{self.NomTab}'""")
        #self.Cursor.execute(f"SELECT * FROM {self.NomTab} INFORMATION_SCHEMA")
        #wip()
        print(f"La table {self.NomTab} comporte {len(self.NomsChamps)} champs : {list(enumerate(self.NomsChamps, 1))}")


    def ins(self, *args): # Méthode d'insertion "Colone <> Valeur" dans la table
        '''
        Exemple : MaTable.ins([{"Champs1":"Valeur1", "Champs2":"Valeur2", etc.}])
        
        Insére pour chaque champs précisés les valeurs précisées.
            WIP : Pour chaque dict une ligne est créée.

        :parametres : args : tuple d'une liste de dictionnaires [{"col1":1, "col2":"test","col3":4},
                                                                    {"col1":1, "col2":"test","col3":4}]
        '''
        pti=f"{self.SQL_Val},"*(len(args[0][0])-1)

        CodeSQL = f'''INSERT INTO {self.NomTab} ('''

        i=1
        ligne_data=[]
        for k, v in enumerate(args[0][0]):
            if i < len(args[0][0]) : 
                CodeSQL += v + ", " #les noms de champs
            elif i == len(args[0][0]) :
                CodeSQL += v
            ligne_data.append(args[0][0][v])   #liste des valeurs a inserer
            i+=1
        CodeSQL += f''') VALUES({pti}{self.SQL_Val});'''
        tpl_data=tuple(ligne_data)
        
        self.Cursor.execute(CodeSQL,tpl_data)
        self.Connector.commit()


    def inli(self, *args): # Méthode d'insertion par ligne dans la table
        '''
        Exemple : MaTable.inli("Valeur1", "Valeur2", "Valeur3", ...)
        Insére les valeurs dans l'ordre des champs de la table.

        :parametres : args : Mes valeurs, à la suite, séparées par des virgules.
        '''
        CodeSQL_D = f"INSERT INTO {self.NomTab} ('"
        CodeSQL_F = "') VALUES ('"
        i = 1
        for Indice, champ in enumerate(self.NomsChamps) :
            if i < len(self.NomsChamps) : 
                CodeSQL_D += champ + "', '"
                CodeSQL_F += args[Indice] + "', '"
            elif i == len(self.NomsChamps) :
                CodeSQL_D += champ
                CodeSQL_F += args[Indice] + "')"
            i+=1

        self.Cursor.execute(CodeSQL_D + CodeSQL_F)
        self.Connector.commit()


    def read(self, col_filtre, val_filtre): # Méthode de consultation de la table
        '''ex : table.read_data("col_1", 5)
        
        Retourne tous les enregistrements de la table
        filtrée sur le champs donné par 'col_filtre' dont la valeur est 'val_filtre'
        '''
        select_sql = f'SELECT * FROM  {self.NomTab} WHERE {col_filtre} = '
        select_sql += f'"{val_filtre}";' if type(val_filtre) ==str else f'{val_filtre};'
        
        self.Cursor.execute(select_sql)
                                                
        rows = self.Cursor.fetchall()
        for row in rows:
            print(row)


    def dele(self, col_filtre, val_filtre): # Méthode de supression de ligne dans la table
        '''ex : table.delete_data("col_1", 5)
        
        Supprime tous les enregistrements de la table
        filtrée sur le champs donné par 'col_filtre' dont la valeur est 'val_filtre'
        :param :
        :return:
        
        '''
        delete_sql = f'DELETE FROM {self.NomTab} WHERE {col_filtre} = '
        delete_sql += f'"{val_filtre}";' if type(val_filtre) ==str else f'{val_filtre};'
        
        self.Cursor.execute(delete_sql)
        self.Connector.commit()


    def upd(self, col_filtre, val_filtre, *nvl_data ):
            '''ex : table.update_data("col_1", 5, {"col_1" : 2,
                                                "col_2" : "test2",
                                                "col_3" : 8})
            
            Met a jour les champs de la table avec de nvles valeurs
            pour l'enregistrement filtré sur le champs donné par 'col_filtre' dont la valeur est 'val_filtre'
            :param = col_filtre, val_filtre : enregistrement à mettre à jour
                    entre { } : (structure de dictionnaire) "champ_a_modifier" : nouvelle_valeur
            :return:
        
            '''
            set_val=""
            i=1
            for k in nvl_data[0]:
                if i < len(nvl_data[0]) : 
                    set_val += k + " = '"+ str(nvl_data[0][k]) + "', "  if type(nvl_data[0][k])==str else k + " = "+ str(nvl_data[0][k]) + ", "

                elif i == len(nvl_data[0]) :
                    set_val += k + " = '"+ str(nvl_data[0][k]) + "'" if type(nvl_data[0][k])==str else k + " = "+ str(nvl_data[0][k])
                i+=1
            
            update_sql = f'UPDATE {self.NomTab} SET {set_val} WHERE {col_filtre} = '
            update_sql += f'"{val_filtre}";' if type(val_filtre) == str else f'{val_filtre};'
            
            self.Cursor.execute(update_sql)
            self.Connector.commit()


    def drop(self): # Méthode supression de la table
        '''
        Exemple : MaTable.drop()
        Supprime la table de la BD.
        '''
        self.Cursor.execute(f"DROP TABLE IF EXISTS {self.NomTab}")
        self.Connector.commit()



""" -------- Autres -------- """
def ErrorAutoSQL(ErrorType = None): # WIP interprétateur d'erreurs d'utilisation du module "AutoSQL"
    if ErrorType == None:
        print("Erreur AutoSQL")
    else:
        print("Erreur AutoSQL : " + str(ErrorType))


def Help(): # Notice dans terminal
    print("""
    ------ Aide module AutoSQL -------

    Outils de création de base de données :

        -> dba(NomBD, URI='Optionel') : Méthode de création rapide par défaut.
            Pour les néophytes, AutoSQL crée une BD SQLite3 dans votre dossier actuel.
            Pour les expérts, une URI permet de configurer votre BD rapidement.

        -> dbs(NomBD, CheminBD='Optionel') : Méthode de création de BD SQLite3 (Par défaut)
            CheminBD perment de définir l'emplacement du fichier NomBD.db dans votre arborescence.

        -> dbp(NomBD, host, port, user, password) : Méthode de création de BD Postgres
            > WIP


    Outils de création de table :

        --> .tab("NomTable", "Champs1", "Type1", "Champs2", "Type2", etc.) : Crée une table
            NomTable détermine le nom de la table dans la BD.
            Les arguments suivants définissent, en alternance, le nom des champs et leurs types / options.


    Outils de manipulations de data :

        ---> .col() : Enumére dans le terminal le nom des champs de l'object table.

        ---> .ins([{"Champs1":"Valeur1", "Champs2":"Valeur2", etc.}]) : Insére pour chaque champs précisés les valeurs précisées.
            WIP : Pour chaques dict une ligne est créée.

        ---> .inli("Valeur1", "Valeur2", "Valeur3", etc.) : Insére les valeurs dans l'ordre des champs de la table.

        ---> .read(col_filtre, val_filtre) : Enumére dans le terminal les enregistrements dont les valeurs correspondent à la val_filtre dans la col_filtre.

        ---> .dele(col_filtre, val_filtre) : Supprime tout les enregistrements dont les valeurs correspondes à la val_filtre dans la col_foltre.

        ---> .upd(col_filtre, val_filtre, {"Champs1":"Valeur1", "Champs2":"Valeur2", etc.}) : Remplace toutes les enregistrements dont les valeurs correspondes à la val_filtre dans la col_foltre par chaque valeurs précisées les champs précisés.

        ---> .drop() : Supprime la table de la BD.

        """)
        