import AutoSQL

""" Voir l'aide du module """
#AutoSQL.Help()

""" Creer / Se connecter à une BD pour débutant """
#MaPremiereDB = AutoSQL.dba("MaPremiereDB")

""" Creer / Se connecter à une BD pour expert """
#MaDBTest = AutoSQL.dba(URI="sqlite:////home/julien/Pro/Projets/05-POO_AutoSQL/MaDBTest.db")

""" Creer 2 tables dans ma BD """
#MaTabTest = MaDBTest.tab("MaTableTest", "ID", "TEXT", "Id_dpt", "TEXT", "nom_commune", "TEXT", "code_postal", "TEXT")
#MaTabTest2 = MaDBTest.tab("MaTableTest2", "Col1", "TEXT", "Col2", "TEXT", "Col3", "TEXT")

""" Creer une BD SQLite avec des parametres & creer une table en une ligne """
#MaDB_TabTest = AutoSQL.dbs("NomTestDBSQLite", CheminBD="Test/").tab("MaTableTest3", "ID", "INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE", "Id_dpt", "TEXT","nom_commune", "TEXT","code_postal", "TEXT")

""" Supprimer une table """
#MaTabTest.drop()

""" Retrouver les noms de champs d'une table """
#MaTabTest2.col()

""" Inserer des données ciblées dans une table """
#MaTabTest2.ins([{"Col1":"Val1", "Col2":"Val2", "Col3":"Val3"}])
#MaTabTest2.ins([{"Col1":"Val4", "Col2":"Val5", "Col3":"Val6"}])
#MaTabTest2.ins([{"Col1":"Val7", "Col2":"Val8", "Col3":"Val9"}])

""" Inserer rapidement des "lignes" de données dans une table """
#MaTabTest2.inli("Val10", "Val11", "Val12")
#MaTabTest2.inli("Val10", "Val11", "Val13")

""" Consulter / filtrer les données d'une table """
#MaTabTest2.read("Col1", "Val10")

""" Modifier les données d'une table """
#MaTabTest2.upd("Col1", "Val1", {"Col1":"Val10", "Col2":"Val14", "Col3":"Val15"})
#MaTabTest2.read("Col1", "Val10")

""" Effacer les données d'une table """
#MaTabTest2.dele("Col1", "Val7")
#MaTabTest2.read("Col1", "Val7")